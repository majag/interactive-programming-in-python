# RiceRocks, Interactive Programming in Python
# by Maja Grubisic

import simplegui
import math
import random

# global variables
WIDTH = 800
HEIGHT = 600
score = 0
lives = 3
time = 0.5
started = False

class ImageInfo:
    def __init__(self, center, size, radius = 0, lifespan = None, animated = False):
        self.center = center
        self.size = size
        self.radius = radius
        if lifespan:
            self.lifespan = lifespan
        else:
            self.lifespan = float('inf')
        self.animated = animated

    def get_center(self):
        return self.center

    def get_size(self):
        return self.size

    def get_radius(self):
        return self.radius

    def get_lifespan(self):
        return self.lifespan

    def get_animated(self):
        return self.animated

    
# art assets created by Kim Lathrop, may be freely re-used in non-commercial projects, please credit Kim
    
# debris images - debris1_brown.png, debris2_brown.png, debris3_brown.png, debris4_brown.png
#                 debris1_blue.png, debris2_blue.png, debris3_blue.png, debris4_blue.png, debris_blend.png
debris_info = ImageInfo([320, 240], [640, 480])
debris_image = simplegui.load_image("http://commondatastorage.googleapis.com/codeskulptor-assets/lathrop/debris2_blue.png")

# nebula images - nebula_brown.png, nebula_blue.png
nebula_info = ImageInfo([400, 300], [800, 600])
nebula_image = simplegui.load_image("http://commondatastorage.googleapis.com/codeskulptor-assets/lathrop/nebula_blue.f2013.png")

# splash image
splash_info = ImageInfo([200, 150], [400, 300])
splash_image = simplegui.load_image("http://commondatastorage.googleapis.com/codeskulptor-assets/lathrop/splash.png")

# ship image
ship_info = ImageInfo([45, 45], [90, 90], 35)
ship_image = simplegui.load_image("http://commondatastorage.googleapis.com/codeskulptor-assets/lathrop/double_ship.png")

# missile image - shot1.png, shot2.png, shot3.png
missile_info = ImageInfo([5,5], [10, 10], 3, 50)
missile_image = simplegui.load_image("http://commondatastorage.googleapis.com/codeskulptor-assets/lathrop/shot2.png")

# asteroid images - asteroid_blue.png, asteroid_brown.png, asteroid_blend.png
asteroid_info = ImageInfo([45, 45], [90, 90], 40)
asteroid_image = simplegui.load_image("http://commondatastorage.googleapis.com/codeskulptor-assets/lathrop/asteroid_blue.png")

# animated explosion - explosion_orange.png, explosion_blue.png, explosion_blue2.png, explosion_alpha.png
explosion_info = ImageInfo([64, 64], [128, 128], 17, 24, True)
explosion_image = simplegui.load_image("http://commondatastorage.googleapis.com/codeskulptor-assets/lathrop/explosion_alpha.png")

# sound assets purchased from sounddogs.com, please do not redistribute
soundtrack = simplegui.load_sound("http://commondatastorage.googleapis.com/codeskulptor-assets/sounddogs/soundtrack.mp3")
missile_sound = simplegui.load_sound("http://commondatastorage.googleapis.com/codeskulptor-assets/sounddogs/missile.mp3")
missile_sound.set_volume(.5)
ship_thrust_sound = simplegui.load_sound("http://commondatastorage.googleapis.com/codeskulptor-assets/sounddogs/thrust.mp3")
explosion_sound = simplegui.load_sound("http://commondatastorage.googleapis.com/codeskulptor-assets/sounddogs/explosion.mp3")


# helper functions to handle transformations
def angle_to_vector(ang):
    return [math.cos(ang), math.sin(ang)]

def dist(p,q):
    return math.sqrt((p[0]-q[0])**2+(p[1]-q[1])**2)

# Ship class
class Ship:
    def __init__(self, pos, vel, angle, image, info):
        self.pos = [pos[0],pos[1]]
        self.vel = [vel[0],vel[1]]
        self.thrust = False
        self.angle = angle
        self.angle_vel = 0
        self.image = image
        self.image_center = info.get_center()
        self.image_size = info.get_size()
        self.radius = info.get_radius()
        
    def draw(self,canvas):
        if self.thrust:
            canvas.draw_image(self.image, [self.image_center[0] + self.image_size[0], self.image_center[1]] , self.image_size,
                              self.pos, self.image_size, self.angle)
        else:
            canvas.draw_image(self.image, self.image_center, self.image_size, self.pos, self.image_size, self.angle)

    def update(self):
        self.angle += self.angle_vel
        self.pos[0] += self.vel[0]
        self.pos[1] += self.vel[1]
        if (self.pos[0] // 800) == 1:
            self.pos[0] = 0
        elif (self.pos[0] // 800) < 0:
            self.pos[0] = 800

        if (self.pos[1] // 600) == 1:
            self.pos[1] = 0
        elif (self.pos[1] // 600) < 0:
            self.pos[1] = 600

        self.vel[0] *= 0.99
        self.vel[1] *= 0.99

        if self.thrust == True:
            self.forward = angle_to_vector(self.angle)
            self.vel[0] += self.forward[0] * 0.1
            self.vel[1] += self.forward[1] * 0.1

    def set_thrust(self, on):
        self.thrust = on
        if on:
            ship_thrust_sound.rewind()
            ship_thrust_sound.play()
        else:
            ship_thrust_sound.pause()
       
    def increment_angle_vel(self):
        self.angle_vel += .05
        
    def decrement_angle_vel(self):
        self.angle_vel -= .05
        
    def shoot(self):
        global missiles
        forward = angle_to_vector(self.angle) 
        missile_pos = [self.pos[0] + self.radius * forward[0], self.pos[1] + self.radius * forward[1]]
        missile_vel = [self.vel[0] + 6 * forward[0], self.vel[1] + 6 * forward[1]]
        a_missile = Sprite(missile_pos, missile_vel, self.angle, 0, missile_image, missile_info, missile_sound)
        missiles.add(a_missile);
        
    def get_radius(self):
        return self.radius;
    
    def get_pos(self):
        return self.pos;

    
# Sprite class
class Sprite:
    def __init__(self, pos, vel, ang, ang_vel, image, info, sound = None):
        self.pos = [pos[0], pos[1]]
        self.vel = [vel[0], vel[1]]
        self.angle = ang
        self.angle_vel = ang_vel
        self.image = image
        self.image_center = info.get_center()
        self.image_size = info.get_size()
        self.radius = info.get_radius()
        self.lifespan = info.get_lifespan()
        self.animated = info.get_animated()
        self.age = 0
        if sound:
            sound.rewind()
            sound.play()
   
    def draw(self, canvas):
        global missiles;
        if self.age <= self.lifespan:
            canvas.draw_image(self.image, self.image_center, self.image_size, self.pos, self.image_size, self.angle)       
        
        if self.animated and self.is_alive():
           self.image_center[0]+=self.image_size[0]

    def get_radius(self):
        return self.radius
    
    def get_pos(self):
        return self.pos
    
    def update(self):
        # update angle
        self.angle += self.angle_vel
        
        # update position
        self.pos[0] = (self.pos[0] + self.vel[0]) % WIDTH
        self.pos[1] = (self.pos[1] + self.vel[1]) % HEIGHT
        self.age += 1
        
    def is_alive(self):
        return self.age <= self.lifespan
    
    def collide(self,diff_objects):
        position1 = self.pos
        position2 = diff_objects.get_pos()
        radius1 = self.radius
        radius2 = diff_objects.get_radius()
        if (dist(position1, position2) <= (radius1 + radius2)):
            return True
        else:
            return False


# key handlers to control ship   
def keydown(key):
    ang_vel = 0.09
    if key == simplegui.KEY_MAP['left']:
        my_ship.angle_vel = - ang_vel
    elif key == simplegui.KEY_MAP['right']:
        my_ship.angle_vel = ang_vel
    elif key == simplegui.KEY_MAP['up']:
        my_ship.thrust = True
        if my_ship.thrust == True:
            sound = ship_thrust_sound
            sound.play()
    elif key == simplegui.KEY_MAP['space']:
        my_ship.shoot()
        
def keyup(key):
    angle_vel = 0
    if key == simplegui.KEY_MAP['right']:
        my_ship.angle_vel = angle_vel
    elif key == simplegui.KEY_MAP['left']:
        my_ship.angle_vel = angle_vel
    elif key == simplegui.KEY_MAP['up']:
        my_ship.thrust = False
        if my_ship.thrust == False:
            my_ship.forward = [0,0]
            sound = ship_thrust_sound
            sound.pause()
        
# mouseclick handlerr
def click(pos):
    global started, score, lives
    lives = 3
    score = 0
    center = [WIDTH / 2, HEIGHT / 2]
    size = splash_info.get_size()
    inwidth = (center[0] - size[0] / 2) < pos[0] < (center[0] + size[0] / 2)
    inheight = (center[1] - size[1] / 2) < pos[1] < (center[1] + size[1] / 2)
    if (not started) and inwidth and inheight:
        started = True

def draw(canvas):
    global time, started,lives
    
    # animiate background
    time += 1
    center = debris_info.get_center()
    size = debris_info.get_size()
    wtime = (time / 8) % center[0]
    canvas.draw_image(nebula_image, nebula_info.get_center(), nebula_info.get_size(), [WIDTH/2, HEIGHT/2], [WIDTH, HEIGHT])
    canvas.draw_image(debris_image, [center[0]-wtime, center[1]], [size[0]-2*wtime, size[1]], [WIDTH/2+1.25*wtime, HEIGHT/2], [WIDTH-2.5*wtime, HEIGHT])
    canvas.draw_image(debris_image, [size[0]-wtime, center[1]], [2*wtime, size[1]], [1.25*wtime, HEIGHT/2], [2.5*wtime, HEIGHT])

    # draw user status
    canvas.draw_text("Lives", [50, 50], 22, "White")
    canvas.draw_text("Score", [680, 50], 22, "White")
    canvas.draw_text(str(lives), [50, 80], 22, "White")
    canvas.draw_text(str(score), [680, 80], 22, "White")

    # draw ship and sprites
    my_ship.draw(canvas)
    process_sprite_group(canvas,rocks)

    if group_collide(rocks,my_ship):
        lives -=1;
    if (lives == 0 and started):
        reset()
        return   
    process_sprite_group(canvas,missiles)
    group_group_collide(rocks,missiles)
    process_sprite_group(canvas,explosion)

    # update ship 
    my_ship.update()
    
    # draw splash screen if not started
    if (not started):       
        canvas.draw_image(splash_image, splash_info.get_center(), splash_info.get_size(), [WIDTH/2, HEIGHT/2], splash_info.get_size())
         

# timer handler that spawns a rock    
def rock_spawner():
    if not started:
        return;
    global rocks;
    if (len(rocks) > 12):
        return
    rock_position = [random.randrange(0, WIDTH), random.randrange(0, HEIGHT)]
    rock_velocity = [random.random() * .6 - .3, random.random() * .6 - .3]
    rock_avel = random.random() * .2 - .1
    a_rock = Sprite(rock_position, rock_velocity, 0, rock_avel, asteroid_image, asteroid_info)
    rocks.add(a_rock)

def process_sprite_group(canvas,set_sprite):
    for sprite in list(set_sprite):
        if sprite.is_alive():
            sprite.draw(canvas);
            sprite.update();
        else:
            set_sprite.remove(sprite)

def group_collide(group,diff_objects):
    flag=False
    for objects in list(group):
        if objects.collide(diff_objects):
            group.remove(objects)
            flag = True
    return flag
    
def group_group_collide(group1,group2):
    global score, explosion
    for objects in list(group2):
        flag = group_collide(group1, objects)
        if(flag): 
            pose = objects.get_pos()
            a_explosion = Sprite(pose, [0 ,0], 0, 0, explosion_image, explosion_info,explosion_sound)           
            explosion.add(a_explosion)
            group2.remove(objects)
            score += 1
            
        
def reset():
    global lives, started, score
    started = False
    rocks.difference_update(rocks)
    missiles.difference_update(missiles)
    explosion.difference_update(explosion)
    soundtrack.rewind()
    soundtrack.play()
        
# initialize frame
frame = simplegui.create_frame("Asteroids", WIDTH, HEIGHT)

my_ship = Ship([WIDTH / 2, HEIGHT / 2], [0, 0], 0, ship_image, ship_info)
rocks=set([])
missiles=set([])
explosion=set([])

# register handlers
frame.set_mouseclick_handler(click)
frame.set_draw_handler(draw)
frame.set_keydown_handler(keydown)
frame.set_keyup_handler(keyup)

timer = simplegui.create_timer(1000.0, rock_spawner)

timer.start()
frame.start()