# Rock-paper-scissors-lizard-Spock template
# implementation by Maja Grubisic
# http://www.codeskulptor.org/#user19_Ixj0HESVdUbASel_0.py

# The key idea of this program is to equate the strings
# "rock", "paper", "scissors", "lizard", "Spock" to numbers
# as follows:
#
# 0 - rock
# 1 - Spock
# 2 - paper
# 3 - lizard
# 4 - scissors

import random

# helper functions

def number_to_name(number):
    # fill in your code below
    # convert number to a name using if/elif/else
    # don't forget to return the result!
    
    if number == 0:
        name = 'rock'
        return name
    elif number == 1:
        name = 'Spock'
        return name
    elif number == 2:
        name = 'paper'
        return name
    elif number == 3:
        name = 'lizard'
        return name
    elif number == 4:
        name = 'scissors'
        return name
    else:
        print'Wrong input, try again'
    
    
def name_to_number(name):
    # fill in your code below
    # convert name to number using if/elif/else
    # don't forget to return the result!
    
    if name == 'rock':
        number = 0
        return number
    elif name == 'Spock':
        number = 1
        return number
    elif name == 'paper':
        number = 2
        return number
    elif name == 'lizard':
        number = 3
        return number
    elif name == 'scissors':
        number = 4
        return number
    else:
        print'Wrong input, try again'

def rpsls(name): 
    # fill in your code below

    player_guess = name
    
    # convert name to player_number using name_to_number
    player_number = name_to_number(name)

    # compute random guess for comp_number using random.randrange()
    # we could also add the implementation of the case when comp_numer is 
    # the same as player_number and resolve it with a while loop to make 
    # sure that numbers are different, but I just wrote that it's a tie
    
    comp_number = random.randrange(0, 5)

    # compute difference of player_number and comp_number modulo five
    difference = (comp_number - player_number) % 5

    computer_guess = number_to_name(comp_number)

    print 'Player chooses', player_guess
    print 'Computer chooses', computer_guess 

    # use if/elif/else to determine winner
    if difference == 1 or difference == 2:
        print 'Computer wins!\n'
    elif difference == 3 or difference == 4:
        print 'Player wins!\n'
    else:
        print "Player and computer tie!\n"

    return 
    
# test your code
rpsls("rock")
rpsls("Spock")
rpsls("paper")
rpsls("lizard")
rpsls("scissors")

# always remember to check your completed program against the grading rubric


