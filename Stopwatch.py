# "Stopwatch: The Game"
# by Maja Grubisic

import simplegui
import math

#global variables
position = [90, 110]
time = 0
minutes = 0
seconds = 0
mil_seconds = 0
interval = 100
success = 0
attempts = 0
game_stop = True

# function  that converts time into formatted string A:BC.D
def format(time):
    global mil_seconds
    global seconds
    global minutes
    minutes = 0
    mil_seconds = time%10
    b = time/10
    minutes = int(b/60)
    seconds = int(b%60)
    if mil_seconds > 9:
        mil_seconds = 0
        seconds += 1
        message = str(minutes)+":"+str(seconds)+"."+str(mil_seconds)
    if seconds > 59:
        minutes += 1
        seconds -= 60       
        message = str(minutes)+":"+str(seconds)+"."+str(mil_seconds)
    if len(str(seconds)) == 1:
        seconds_str = "0" + str(seconds)
        message = str(minutes)+":"+seconds_str+"."+str(mil_seconds)
    else:
        message = str(minutes)+":"+str(seconds)+"."+str(mil_seconds)
    return message
    
# event handlers for buttons
def start():
    global game_stop
    
    if game_stop == True:
        game_stop = False
        timer.start()
       
def stop():
    global game_stop 
    global success 
    global attempts
    
    if game_stop == False:
        game_stop = True
        timer.stop()
        if (mil_seconds % 10 == 0):
            success += 1
        attempts += 1
       
def reset():
    global time
    global game_stop
    global success
    global attempts
    
    if game_stop == False:
        game_stop = True
        timer.stop()
    time = 0
    success = 0
    attempts = 0
    #timer.start()

# event handler for timer with 0.1 sec interval
def time_track():
    global time
    time += 1

# define draw handler
def draw(canvas):
    canvas.draw_text(format(time), position, 50, "Red")
    canvas.draw_text(str(success)+"/"+str(attempts), [220, 20], 20, "White")
    
# create frame
frame = simplegui.create_frame("Stopwatch: The Game", 300, 200)
timer = simplegui.create_timer(interval, time_track)

# register event handlers
frame.set_draw_handler(draw)
frame.add_button("Start", start, 200)
frame.add_button("Stop", stop, 200)
frame.add_button("Reset", reset, 200)

# start frame
frame.start()

