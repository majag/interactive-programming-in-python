# mini-project "Guess the number"
# by Maja Grubisic

import random
import simplegui

# initialize global variables used in your code

attempts = 7
stop_range = 100

# helper function to start and restart the game
def new_game():
    # remove this when you add your code    
    global secret_number
    global stop_range
    global attempts
    if stop_range == 100:
        attempts = 7
    else:
        attempts = 10
        
    secret_number = random.randrange(0, stop_range)
    print "New game. Range is from 0 to", stop_range,"\nNumber of remaining guesses is", attempts

# define event handlers for control panel
def range100():
    # button that changes range to range [0,100) and restarts
    global stop_range
    stop_range = 100
    new_game()

def range1000():
    # button that changes range to range [0,1000) and restarts
    global stop_range
    stop_range = 1000
    new_game()
    
def input_guess(guess):
    # main game logic goes here 
    print "Guess was", guess_number,"\nNumber of remaining guesses is", attempts
    if attempts == 0:
        print "You ran out of guesses. The number was", secret_number
        new_game()
    elif secret_number > guess_number:
        print "Higher!"
    elif secret_number < guess_number:
        print "Lower!"
    else:
        print "You've guessed!!!"
        new_game()
    
# enter box    
def enter(num):
    global guess_number
    global attempts
    attempts -= 1
    guess_number = int(num)
    input_guess(guess_number)
    
# create frame
f = simplegui.create_frame("Guess the number!", 300, 300)

# register event handlers for control elements
f.add_button("Range is [0, 100)", range100, 200)
f.add_button("Range is [0, 1000)", range1000, 200)
f.add_input("Enter a guess", enter, 200)

# call new_game and start frame
new_game()
f.start()

