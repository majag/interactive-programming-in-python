#Collatz conjecture and timers in Python
#by Maja Grubisic

import simplegui
import random
n = 217

def click():
    timer.start()
    
def input_handler(text):
    global n
    n = int(text)
    
def timer_handler():
    global n
    if n == 1:
        timer.stop()
    elif n % 2 == 0:
        n = n/2
        print n
    else:
        n = n * 3 + 1
        print n

        
def draw(canvas):
    canvas.draw_text(str(n), [120, 117], 80, "red")
    
frame = simplegui.create_frame("Collatz conjecture", 400, 400)
timer = simplegui.create_timer(500, timer_handler)
frame.add_button("Start", click)
frame.set_draw_handler(draw)
#inp = frame.add_input("Number", input_handler, 50)
frame.start()