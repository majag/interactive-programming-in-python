# Implementation of classic arcade game Pong
# by Maja Grubisic

import simplegui
import random

# global variables
WIDTH = 600
HEIGHT = 400       
BALL_RADIUS = 20
PAD_WIDTH = 8
PAD_HEIGHT = 80
HALF_PAD_WIDTH = PAD_WIDTH / 2
HALF_PAD_HEIGHT = PAD_HEIGHT / 2
LEFT = False
RIGHT = True

# ball positions 
BALL_POS = [WIDTH / 2,HEIGHT / 2];
BALL_VEL = [random.randrange(5,10), -random.randrange(4,8)];

# paddle positions
PADDLE2_POS = HEIGHT / 2;
PADDLE1_POS = HEIGHT / 2;
PADDLE1_VEL = PADDLE2_VEL = 0;

# initialize BALL_POS and BALL_VEL for new ball in the middle of the table
# if direction is right, the ball's velocity is upper right, else upper left

def spawn_ball(direction):
    global BALL_POS, BALL_VEL, HITS # these are vectors stored as lists
    BALL_POS = [WIDTH / 2, HEIGHT / 2];
    if (direction == True):
        BALL_VEL = [random.randrange(5,10), -random.randrange(4,8)];
    else:
        BALL_VEL = [-random.randrange(5,10), -random.randrange(4,8)];

# define event handlers

def new_game():
    global PADDLE1_POS, PADDLE2_POS, PADDLE1_VEL, PADDLE2_VEL  # these are numbers
    global SCORE1, SCORE2  # these are ints
    ch = random.randrange(0,2);
    if (ch == 0):
        spawn_ball(False);
    else:
        spawn_ball(True);

    #paddle values
    PADDLE2_POS = HEIGHT / 2;
    PADDLE1_POS = HEIGHT / 2;
    PADDLE1_VEL = PADDLE2_VEL = 0;

    #scores
    SCORE1=SCORE2=0; 

def draw(c):
    global SCORE1, SCORE2, PADDLE1_POS, PADDLE2_POS, BALL_POS, BALL_VEL 
        
    # draw mid line and gutters
    c.draw_line([WIDTH / 2, 0],[WIDTH / 2, HEIGHT], 1, "White")
    c.draw_line([PAD_WIDTH, 0],[PAD_WIDTH, HEIGHT], 1, "White")
    c.draw_line([WIDTH - PAD_WIDTH, 0],[WIDTH - PAD_WIDTH, HEIGHT], 1, "White")
        
    # update ball
    x1 = BALL_POS[0] + BALL_VEL[0];
    x2 = BALL_POS[1] + BALL_VEL[1];
    
    if (x2 < BALL_RADIUS):
        x2 = BALL_RADIUS;
        BALL_VEL[1] = -BALL_VEL[1];
    elif (x2 > HEIGHT-1-BALL_RADIUS):
        x2 = HEIGHT-1-BALL_RADIUS;
        BALL_VEL[1] = -BALL_VEL[1];
    if (x1 < BALL_RADIUS+PAD_WIDTH):
       if (x2 < PADDLE1_POS+HALF_PAD_HEIGHT and x2 > PADDLE1_POS-HALF_PAD_HEIGHT):
            x1 = BALL_RADIUS+PAD_WIDTH+5;
            BALL_VEL[0] = -BALL_VEL[0]*1.1;
            BALL_VEL[1] = BALL_VEL[1]*1.1;
       else:
            SCORE2 = SCORE2 + 1;
            ch = random.randrange(0, 2);
            if (ch == 0):
                spawn_ball(False);
            else:
                spawn_ball(True);
            return;    
    elif(x1 > WIDTH-1-BALL_RADIUS-PAD_WIDTH) :  
        if(x2 < PADDLE2_POS+HALF_PAD_HEIGHT and x2 > PADDLE2_POS-HALF_PAD_HEIGHT):
            x1 = WIDTH-1-BALL_RADIUS-PAD_WIDTH-5;
            BALL_VEL[0] = -BALL_VEL[0]*1.1;
            BALL_VEL[1] = BALL_VEL[1]*1.1;
        else:
            SCORE1 = SCORE1+1;
            ch = random.randrange(0,2);
            if (ch == 0):
                spawn_ball(False);
            else:
                spawn_ball(True);
            return;
    
    
    BALL_POS[0] = x1;
    BALL_POS[1] = x2;    

    # draw ball
    c.draw_circle(BALL_POS,BALL_RADIUS,2,"Green","Yellow"); 
    c.draw_text(str(SCORE1),[WIDTH /4 ,100],50,"White");

    # update paddle's vertical position, keep paddle on the screen
    x1=PADDLE1_POS+PADDLE1_VEL;
    if(x1<HALF_PAD_HEIGHT):
        x1=HALF_PAD_HEIGHT;
    elif(x1>HEIGHT-HALF_PAD_HEIGHT-1):
        x1=HEIGHT-1-HALF_PAD_HEIGHT
    x2=PADDLE2_POS+PADDLE2_VEL;
    if(x2<HALF_PAD_HEIGHT):
        x2=HALF_PAD_HEIGHT;
    elif(x2>HEIGHT-HALF_PAD_HEIGHT-1):
        x2=HEIGHT-1-HALF_PAD_HEIGHT
    PADDLE1_POS=x1;
    PADDLE2_POS=x2; 

    # draw paddles
    c.draw_polygon([[0, PADDLE1_POS+HALF_PAD_HEIGHT], [PAD_WIDTH,PADDLE1_POS+HALF_PAD_HEIGHT ], [PAD_WIDTH, PADDLE1_POS-HALF_PAD_HEIGHT],[0,PADDLE1_POS-HALF_PAD_HEIGHT]], 1, "Green", "Red");
    c.draw_polygon([[WIDTH-1, PADDLE2_POS+HALF_PAD_HEIGHT], [WIDTH-1-PAD_WIDTH,PADDLE2_POS+HALF_PAD_HEIGHT ], [WIDTH-1-PAD_WIDTH, PADDLE2_POS-HALF_PAD_HEIGHT],[WIDTH-1,PADDLE2_POS-HALF_PAD_HEIGHT]], 1, "Green", "Blue");
    
    # draw SCOREs
    c.draw_text(str(SCORE2),[WIDTH-WIDTH/4,100],50,"White");
    
def keydown(key):
    global PADDLE1_VEL, PADDLE2_VEL
    temp=10

    #player1
    if(key==simplegui.KEY_MAP["W"]):
        PADDLE1_VEL=-temp;
    if(key==simplegui.KEY_MAP["S"]):
        PADDLE1_VEL=temp;

    #player2
    if(key==simplegui.KEY_MAP["up"]):
        PADDLE2_VEL=-temp;
    if(key==simplegui.KEY_MAP["down"]):
        PADDLE2_VEL=temp;
    
   
def keyup(key):
    global PADDLE1_VEL, PADDLE2_VEL

    #player1
    if(key==simplegui.KEY_MAP["W"]):
        PADDLE1_VEL=0;
    if(key==simplegui.KEY_MAP["S"]):
        PADDLE1_VEL=0;

    #player2
    if(key==simplegui.KEY_MAP["up"]):
        PADDLE2_VEL=0;
    if(key==simplegui.KEY_MAP["down"]):
        PADDLE2_VEL=0;
    
    

# create frame
frame = simplegui.create_frame("Pong", WIDTH, HEIGHT)
frame.set_canvas_background('Green')
frame.set_draw_handler(draw)
frame.set_keydown_handler(keydown)
frame.set_keyup_handler(keyup)
frame.add_button("Restart", new_game, 100)


# start frame
new_game()
frame.start()
